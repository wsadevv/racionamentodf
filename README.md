# Racionamento DF #

Racionamento DF foi criado para facilitar a consulta por parte da população aos racionamentos de água programados pela Companhia de Abastecimento de Brasíla (CAESB). 
Utilizando dados provenientes da própria companhia, o aplicativo fornece dados através de duas abordagens: uma via página web, onde é mostrada a situação das regiões administrativas em tempo real e outra, através do parsing das planilhas de racionamento disponíveis no site da CAESB. 
O aplicativo utiliza JSON para a funcionalidade de parsing e uma webview personalizada para aumentar a velocidade de carregamento do mapa interativo. 

Para a criação dos dados em JSON, foi utilizado um script em Python, de autoria própria, para percorrer os campos da planilha e fazer o parse dos dados. 

## Bibliotecas utilizadas 
* JodaTime, disponível em: [JodaTime](http://www.joda.org/joda-time/)
* Advanced Webview, disponível em: [Advanced Webview](https://android-arsenal.com/details/1/4873L)
* Clickable Areas Images, disponível em:[ClickableAreasImages](https://github.com/Lukle/ClickableAreasImages) 
* Gson, solução do Google para parsing de JSON, disponível em: 
* Fancy Buttons, disponível em: [Fancy Buttons](https://android-arsenal.com/details/1/681)

## Tecnologias Utilizadas 
* Python 3.6
* Atom Editor
* Sublime Text 3
* Openpyxl - para edição de arquivos xls, xlsx.