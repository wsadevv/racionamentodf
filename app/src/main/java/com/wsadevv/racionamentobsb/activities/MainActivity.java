package com.wsadevv.racionamentobsb.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import com.wsadevv.racionamentobsb.R;

import mehdi.sakout.fancybuttons.FancyButton;

public class MainActivity extends AppCompatActivity {
    FancyButton appCompatButton;
    FancyButton goPicture;
    AppCompatTextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //get my typefaces
        Typeface typeface = Typeface.createFromAsset(getAssets(), "BebasNeue.ttf");
        textView = (AppCompatTextView) findViewById(R.id.meuTexto);
        textView.setTypeface(typeface);


        appCompatButton = (FancyButton) findViewById(R.id.myButtonID);
        goPicture = (FancyButton) findViewById(R.id.botao_fotoID);



        goPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 showPicture();
            }
        });
        appCompatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadWebView();
            }
        });

    }

    private void showPicture() {
        AlertDialog.Builder alertDialog =  new AlertDialog.Builder(this).setTitle("Atenção").setMessage("Cique no centro do círculo da bacia alimentadora que abastece sua cidade").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(MainActivity.this, PictureActivity.class);
                startActivity(intent);
            }
        }).setIcon(R.drawable.torneira_icon);
        alertDialog.setCancelable(false);
        alertDialog.show();

    }

    public void loadWebView(){
        Intent intent = new Intent(MainActivity.this, RacionaDataActivity.class);
        startActivity(intent);
    }
}
