package com.wsadevv.racionamentobsb.activities;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.wsadevv.racionamentobsb.R;
import com.wsadevv.racionamentobsb.beans.Racionamento;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.Calendar;

public class NotificationActivity extends Activity {
    LocalDate amanha = LocalDate.now().plusDays(1);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        final ArrayList<Racionamento> racionamentoArrayList  =
                (ArrayList<Racionamento>) getIntent().getSerializableExtra("arraySM");
        for (Racionamento racionamento: racionamentoArrayList) {

            if(racionamento.isTemAmanha()){
                Intent intent = agendador(amanha,racionamento);
                PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,0);
                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(this)
                                .setSmallIcon(R.drawable.torneira_icon)
                                .setContentTitle("Fique atento")
                                .setDefaults(NotificationCompat.DEFAULT_SOUND)
                                .setContentText("Racionamento amanhã nas seguintes localidades: "+racionamento.getLocalidades())
                                .addAction(R.drawable.calendar,"Adicionar lembrete",pendingIntent);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle());
                int mNotificationId = 001;


                NotificationManager mNotifyMgr =(NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                mNotifyMgr.notify(mNotificationId, mBuilder.build());
                finish();
            }
        }


    }

    private Intent agendador(LocalDate amanha, Racionamento racionamento) {
        Calendar beginTime = Calendar.getInstance();
        beginTime.set(amanha.year().get(),amanha.monthOfYear().get(),amanha.dayOfMonth().get());
        beginTime.set(Calendar.HOUR_OF_DAY,8);
        System.out.println("ues");
        Log.i("Amanha","Y "+amanha.year().get()+"D "+amanha.dayOfMonth().get()+"M"+amanha.monthOfYear().get());
        Calendar endTime = Calendar.getInstance();
        LocalDate depoisAmanha = amanha.plusDays(1);
        endTime.set(depoisAmanha.year().get(),depoisAmanha.monthOfYear().get(),depoisAmanha.dayOfMonth().get());
        endTime.set(Calendar.HOUR_OF_DAY,8);
        Log.i("Amanha","Y "+depoisAmanha.year().get()+"D "+depoisAmanha.dayOfMonth().get()+"M"+depoisAmanha.monthOfYear().get());
        Intent intent = new Intent(Intent.ACTION_INSERT).setData(CalendarContract.Events.CONTENT_URI).
                putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis()).
                putExtra(CalendarContract.EXTRA_EVENT_END_TIME,endTime.getTimeInMillis()).
                putExtra(CalendarContract.Events.TITLE,"Racionamento").
                putExtra(CalendarContract.Events.DESCRIPTION,racionamento.getLocalidades());
        return intent;
    }

}
