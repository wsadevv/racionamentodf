package com.wsadevv.racionamentobsb.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.wsadevv.racionamentobsb.R;
import com.wsadevv.racionamentobsb.beans.Descoberto;
import com.wsadevv.racionamentobsb.beans.Racionamento;
import com.wsadevv.racionamentobsb.beans.SantaMaria;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import at.lukle.clickableareasimage.ClickableArea;
import at.lukle.clickableareasimage.ClickableAreasImage;
import at.lukle.clickableareasimage.OnClickableAreaClickedListener;
import uk.co.senab.photoview.PhotoViewAttacher;

public class PictureActivity extends AppCompatActivity implements OnClickableAreaClickedListener{
    ImageView imageView;
    RequestQueue requestQueue;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture);
        imageView = (ImageView) findViewById(R.id.baciasRacionamentoID);
        imageView.setImageResource(R.drawable.mapa);
        requestQueue = Volley.newRequestQueue(this);

        ClickableAreasImage clickableAreasImage = new ClickableAreasImage(new PhotoViewAttacher(imageView),this);
        List<ClickableArea> clickableAreas = new ArrayList<>();
        // parameter values (pixels): (x coordinate, y coordinate, width, height) and assign an object to it
        //SM
        clickableAreas.add(new ClickableArea(450, 400, 216,241,new SantaMaria()));
        //Descoberto
        clickableAreas.add(new ClickableArea(100,700,234,241,new Descoberto()));

        clickableAreasImage.setClickableAreas(clickableAreas);



    }

    @Override
    public void onClickableAreaTouched(Object o) {
        if(o instanceof SantaMaria){
            List<Racionamento> racionamentosSM = new ArrayList<>();
            santaMariaParser(racionamentosSM);
            //chama activity
        }else if(o instanceof Descoberto){
            List<Racionamento> racionamentosDB = new ArrayList<>();
            descobertoParser(racionamentosDB);
        }
    }

    private void descobertoParser(final List<Racionamento> racionamentosDB) {
        Toast.makeText(this, "Aguarde...", Toast.LENGTH_SHORT).show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, getString(R.string.descoberto_url),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("racionamentos");
                            for(int i = 0; i< jsonArray.length(); i++){
                                JSONObject racionamento = jsonArray.getJSONObject(i);
                                String locais = racionamento.getString("locais");
                                String data = racionamento.getString("data");
                                Racionamento r = new Racionamento();
                                r.setLocalidades(locais);
                                r.setData(data);
                                r.setBacia("Descoberto");
                                racionamentosDB.add(r);




                            }
                            Intent intent = new Intent(PictureActivity.this, ShowDescobertoActivity.class);
                            intent.putExtra("arrayDescoberto",(Serializable) racionamentosDB);
                            startActivity(intent);
                        } catch (Exception e) {
                            Log.e("Exception",e.getMessage());
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Volley", error.getMessage());
                    }
                }

        );
        requestQueue.add(jsonObjectRequest);
    }


    public void santaMariaParser(final List<Racionamento> racionamentosSM) {
        Toast.makeText(this, "Aguarde...", Toast.LENGTH_SHORT).show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,getString(R.string.santaMaria_url),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("racionamentos");
                            for(int i = 0; i< jsonArray.length(); i++){
                                JSONObject racionamento = jsonArray.getJSONObject(i);
                                String locais = racionamento.getString("locais");
                                String data = racionamento.getString("data");
                                Racionamento r = new Racionamento();
                                String bacia = "Santa Maria";
                                r.setData(data);
                                r.setLocalidades(locais);
                                r.setBacia(bacia);
                                racionamentosSM.add(r);
                            }
                            Intent intent = new Intent(PictureActivity.this, ShowSantaMariaActivity.class);
                            intent.putExtra("arraySM", (Serializable) racionamentosSM);
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("Exception",e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            Log.e("Volley", error.getMessage());
                        }catch (NullPointerException e){
                            Log.i("Erro no volley: ",e.getMessage());
                        }

                    }
                }

        );
        requestQueue.add(jsonObjectRequest);

    }
}
