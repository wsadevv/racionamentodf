package com.wsadevv.racionamentobsb.activities;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.wsadevv.racionamentobsb.R;

import im.delight.android.webview.AdvancedWebView;

public class RacionaDataActivity extends AppCompatActivity implements AdvancedWebView.Listener {
    private AdvancedWebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_raciona_data);
        webView = (AdvancedWebView) findViewById(R.id.myWebView);
        webView.setListener(this, this);
        webView.loadUrl(getString(R.string.url_mapa_caesb));

    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {

    }

    @Override
    public void onPageFinished(String url) {

    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {

    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }
}
