package com.wsadevv.racionamentobsb.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.wsadevv.racionamentobsb.R;
import com.wsadevv.racionamentobsb.adapter.RacionamentoAdapter;
import com.wsadevv.racionamentobsb.beans.Racionamento;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;

public class ShowDescobertoActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_descoberto);
        final ArrayList<Racionamento> racionamentoArrayList  = (ArrayList<Racionamento>) getIntent().getSerializableExtra("arrayDescoberto");
        LocalDate localDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("EEEE dd/MMM");
        String dataString = localDate.toString(formatter);
        Log.i("Hoje: ",dataString);
        dataString = dataString.trim();
        for (Racionamento racionamento: racionamentoArrayList) {
            //dataString.toLowerCase().equals(racionamento.getData().toLowerCase().trim()
            if(dataString.toLowerCase().equals(racionamento.getData().toLowerCase().trim())){
                racionamento.setRacionavel(true);
            } else{
                racionamento.setRacionavel(false);
            }
        }

        recyclerView = (RecyclerView) findViewById(R.id.myRecyclerDescobertoID);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        AlertDialog.Builder alertDialog =  new AlertDialog.Builder(this).setTitle("Atenção").
                setMessage("Os resultados marcados com o ícone mostram onde haverá racionamento hoje!").
                setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                recyclerView.setLayoutManager(linearLayoutManager);
                RacionamentoAdapter racionamentoAdapter = new RacionamentoAdapter(racionamentoArrayList);

                recyclerView.setAdapter(racionamentoAdapter);
            }
        }).setIcon(R.drawable.closed_faucet);
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
}
