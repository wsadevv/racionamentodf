package com.wsadevv.racionamentobsb.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.gson.Gson;
import com.wsadevv.racionamentobsb.R;
import com.wsadevv.racionamentobsb.adapter.RacionamentoAdapter;
import com.wsadevv.racionamentobsb.beans.Racionamento;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

public class ShowSantaMariaActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_santa);

        final ArrayList<Racionamento> racionamentoArrayList  = (ArrayList<Racionamento>) getIntent().getSerializableExtra("arraySM");
        LocalDate localDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("EEEE dd/MMM");
        String dataString = localDate.toString(formatter);
        String amanha = localDate.plusDays(1).toString(formatter);
        Log.i("Hoje: ",dataString);
        dataString = dataString.trim();
        int i = 0;
        for (Racionamento racionamento: racionamentoArrayList) {
            if(dataString.toLowerCase().equals(racionamento.getData().toLowerCase().trim())){
                racionamento.setRacionavel(true);
            } else{
              racionamento.setRacionavel(false);
            }
            if(amanha.toLowerCase().equalsIgnoreCase(racionamento.getData().toLowerCase().trim())){
                racionamento.setTemAmanha(true);
            }

        }
        recyclerView = (RecyclerView) findViewById(R.id.myRecyclerSantaID);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        AlertDialog.Builder alertDialog =  new AlertDialog.Builder(this).setTitle("Atenção").setMessage("Os resultados marcados com o ícone mostram onde haverá racionamento hoje!").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                recyclerView.setLayoutManager(linearLayoutManager);
                RacionamentoAdapter racionamentoAdapter = new RacionamentoAdapter(racionamentoArrayList);
                recyclerView.setAdapter(racionamentoAdapter);
                callNotification(racionamentoArrayList);

            }
        }).setIcon(R.drawable.closed_faucet);
        alertDialog.setCancelable(false);
        alertDialog.show();



        //04-13 11:37:27.342 7204-7204/com.wsadevv.racionamentobsb I/Hoje:: qui 13/04
        //04-13 11:37:27.342 7204-7204/com.wsadevv.racionamentobsb I/Data:: Domingo 09/abr

    }
    private void callNotification(ArrayList<Racionamento> racionamentoArrayList) {

        Intent intent = new Intent(ShowSantaMariaActivity.this, NotificationActivity.class);
        intent.putExtra("arraySM",racionamentoArrayList);
        startActivity(intent);

    }


}
