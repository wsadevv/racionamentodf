package com.wsadevv.racionamentobsb.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wsadevv.racionamentobsb.R;
import com.wsadevv.racionamentobsb.beans.Racionamento;

import java.util.List;

/**
 * Created by willi on 11/04/2017.
 */

public class RacionamentoAdapter extends RecyclerView.Adapter<RacionamentoAdapter.RacionamentoViewHolder> {
    List<Racionamento> racionamentoList;

    public RacionamentoAdapter(List<Racionamento> racionamentoList){
        this.racionamentoList = racionamentoList;
    }



    public static class RacionamentoViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView locais;
        TextView data;
        ImageView image;

        RacionamentoViewHolder(View itemView){
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.myCardViewID);
            locais = (TextView) itemView.findViewById(R.id.locaisRacionamentoID);
            data = (TextView) itemView.findViewById(R.id.dataRacionamentosID);
            image = (ImageView) itemView.findViewById(R.id.emRacionamentoID);

        }
    }
    @Override
    public RacionamentoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item,parent,false);
        RacionamentoViewHolder rvh = new RacionamentoViewHolder(v);
        return rvh;
    }

    @Override
    public void onBindViewHolder(RacionamentoViewHolder holder, int position) {
        holder.locais.setText(racionamentoList.get(position).getLocalidades());
        holder.data.setText(racionamentoList.get(position).getData());
        if(racionamentoList.get(position).isRacionavel()){
            holder.image.setImageResource(R.drawable.closed_faucet);
        }else{
            holder.image.setImageDrawable(null);
        }
    }

    @Override
    public int getItemCount() {
        return racionamentoList.size();
    }
}
