package com.wsadevv.racionamentobsb.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by willi on 11/04/2017.
 */

public class Descoberto {
    public Descoberto(){
        this.racionamentos = new ArrayList<>();
    }
    public List<Racionamento> getRacionamentos() {
        return racionamentos;
    }

    public void setRacionamentos(List<Racionamento> racionamentos) {
        this.racionamentos = racionamentos;
    }

    List<Racionamento> racionamentos;

}
