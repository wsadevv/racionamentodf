package com.wsadevv.racionamentobsb.beans;

import java.io.Serializable;



/**
 * Created by willi on 10/04/2017.
 */

public class Racionamento implements Serializable {

    private int id;
    private String localidades;
    private String data;
    private String bacia;
    private int imagem;

    private boolean temAmanha;

    public boolean isRacionavel() {
        return racionavel;
    }

    public void setRacionavel(boolean racionavel) {
        this.racionavel = racionavel;
    }

    private boolean racionavel;

    public int getImagem() {
        return imagem;
    }

    public void setImagem(int imagem) {
        this.imagem = imagem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Racionamento that = (Racionamento) o;

        if (id != that.id) return false;
        return bacia != null ? bacia.equals(that.bacia) : that.bacia == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (bacia != null ? bacia.hashCode() : 0);
        return result;
    }

    public String getBacia() {
        return bacia;
    }

    public void setBacia(String bacia) {
        this.bacia = bacia;
    }

    public Racionamento() {
    }

    public String getLocalidades() {
        return localidades;
    }

    public void setLocalidades(String localidades) {
        this.localidades = localidades;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public boolean isTemAmanha() {
        return temAmanha;
    }

    public void setTemAmanha(boolean temAmanha) {
        this.temAmanha = temAmanha;
    }
}
