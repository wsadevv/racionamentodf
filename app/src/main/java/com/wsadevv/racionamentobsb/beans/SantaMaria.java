package com.wsadevv.racionamentobsb.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by willi on 11/04/2017.
 */

public class SantaMaria {
    public SantaMaria(){
        this.racionamentoList = new ArrayList<>();
    }

    public List<Racionamento> getRacionamentoList() {
        return racionamentoList;
    }

    public void setRacionamentoList(List<Racionamento> racionamentoList) {
        this.racionamentoList = racionamentoList;
    }

    private List<Racionamento> racionamentoList;


}
